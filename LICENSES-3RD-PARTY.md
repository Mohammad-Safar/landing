CONTENT - 3rd Party Licenses
===============================================================================

Death to the Stock Photo
------------------------------------------------------------
Death to Stock is a subscription service for artists that produces premium photo, video, graphic and media downloads.

  - Website: <https://deathtothestockphoto.com/>
  - License: <https://deathtothestockphoto.com/plain-english-license/> - See their "Official License" linked to on that page, which has been reproduced in this 

Each of the photos we have used from Death to the Stock Photo have been modified by reducing the image's dimension and/or quality.  This is done to reduce their overall file size and make them appropriate for use on the web.  The original photos can be found in the `/assets/img/` directory.  They have the same file name, with `.orig` appended just before the file type (ex: `assets/img/example.orig.jpg` => crop => `/static/img/example.jpg`).  The Death to the Stock Photos we use can be found at:

  - `/static/img/benefit-discover-bg.jpg`
  - `/static/img/benefit-knowledge-bg.jpg`
  - `/static/img/benefit-streamline-bg.jpg`
  - `/static/img/benefit-transparency-bg.jpg`
  - `/static/img/benefit-trust-bg.jpg`
  - `/static/img/how-it-works-bg.jpg`
  - `/static/img/intro-bg.jpg`

Font Awesome (Free)
------------------------------------------------------------
See the entry in the "CODE - 3rd Party Licenses" below.


CODE - 3rd Party Licenses
===============================================================================

Font Awesome (Free)
------------------------------------------------------------
The internet's most popular icon toolkit.

  - Website: <https://fontawesome.com/>
  - Code: <https://github.com/FortAwesome/Font-Awesome>
  - License: <https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt>
      * Icons: CC BY 4.0
      * Fonts: SIL OFL 1.1
      * Code: MIT
  - Authors:
      * [Dave Gandy](https://github.com/davegandy)
      * [Travis Chase](https://github.com/supercodepoet)
      * [Rob Madole](https://github.com/robmadole)
      * [Brian Talbot](https://github.com/talbs)
      * [Jory Raphael](https://github.com/sensibleworld)
      * [Mike Wilkerson](https://github.com/mlwilkerson)
      * [Geremia Taglialatela](https://github.com/tagliala)

Hugo
------------------------------------------------------------
A Fast and Flexible Static Site Generator

  - Website: <https://gohugo.io/>
  - Code: <https://github.com/gohugoio/hugo>
  - License: [Apache 2.0](https://github.com/gohugoio/hugo/blob/master/LICENSE)
  - Authors:
      * [bep](https://github.com/bep)
      * [spf13](http://spf13.com/)
      * [friends](https://github.com/gohugoio/hugo/graphs/contributors)

jQuery
------------------------------------------------------------
A a fast, small, and feature-rich JavaScript library.  We use the _slim_ build which excludes ajax and effects.

  - Website: <http://jquery.com/>
  - Code: <https://github.com/jquery/jquery>
  - License: [MIT](https://github.com/jquery/jquery/blob/master/LICENSE.txt)
  - Authors: <https://github.com/jquery/jquery/blob/master/AUTHORS.txt>

Kube
------------------------------------------------------------
A CSS framework for professional developers and designers alike.

  - Website: <https://imperavi.com/kube/>
  - Code: <https://github.com/imperavi/kube>
  - License: [MIT](https://github.com/imperavi/kube/blob/master/LICENSE.md)
  - Authors: [IMPERAVI](https://imperavi.com/)
