---
# Landing: The welcome site for the Distribute Aid software service.
# Copyright (C) 2018 - J. Taylor Fairbank (taylor@distributeaid.org)
#
# Licensed under the AGPLv3. See `/LICENSE.md` and `/LICENSES-3RD-PARTy.md`.

title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
lastmod: {{ .Date }}

draft: true
---

