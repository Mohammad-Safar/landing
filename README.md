Distribute Aid - Landing
===============================================================================

The welcome site for the [Distribute Aid](distributeaid.org) software service.

Copyright & Licensing
------------------------------------------------------------
So it turns out that copyright and licensing can be really complex for a website that has original and third-party content, and original and third-party code.  We've done our best to document these complexities clearly: 

  - Below you'll find an overview of the situation and guidance on how to comply with all the relevant licensing.  This is meant to be human friendly and is not legally binding / does not override any of the actual licenses.  I am not a lawyer.
  - The `/LICENSE.md` file describes the licenses we've released our content and code under.
  - The `/LICENSES-3RD-PARTY.md` lists all the third-party content and code we use, and links to their websites, source, and licenses.
  - The `/LICENSE-DEATH-TO-THE-STOCK-PHOTO.pdf` is a copy of a non-standard third-party content license which is reproduced here to ensure it's availability.

### Content

Our content (text and images) is released under the CC-BY-ND-NC 4.0 license.  You may share our content as long as you credit us, but you can't make derivative works or do so for commercial purposes.

We do use content from third-parties that is not compatible with this license.  All third party content is clearly listed in the `/LICENSES-3RD-PARTY.md` file, along with relevant licensing information.

You should be fine when sharing complete content that includes both our content and third party content, such as sending a screenshot of the website to a friend or uploading the website to an archival service.

Please do not use our content in your own project (a derivative work).  We've put a lot of thought and care into creating it, and would like to protect our project's brand.  You may use the third-party content (such as stock images) if you have a license to use it- see their website and read their licenses to find out if that is the case.

### Code

Our code (html, css, javascript, configuration files, etc) is released under the AGPLv3 license.  You may share it or use it in your own projects as long as you also license those projects with the AGPLv3 license, document any changes, an make the source code available.  Learn more about the AGPLv3 license on the Free Software Foundation's website: <http://www.gnu.org/licenses/agpl-3.0.html>.

We do use code from third-party libraries and frameworks.  We've taken care to ensure that all third-party code is license under a GPLv3 compatible license.  You may also use the third-party code by itself in your own project, in which case you should use the original license that comes with it.

If you wish to use our code for your own project, please be sure to replace our content with your own.  You may also have to replace the third party content (such as stock images) if you do not have a license to use them. 
